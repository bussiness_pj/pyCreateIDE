# Python spider for Sina news channel

1. Use requests to open link, and beautiful soup for parser the html object.

	```python
	# -*- coding:UTF-8 -*-
	import os
	import requests
	from bs4 import BeautifulSoup
	```

2. Url link 

	```python
	url_link = "http://news.sina.com.cn/"
	```

3. Fake a header for client browser 

    ```python
    headers = {"User-Agent": "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36"}
    ```
		    
4. Get html

	```python
	req = requests.get(url=url_link, headers=headers)
	req.encoding = 'utf-8'
	html = req.text
	```
5. Use BeautifulSoup find target
		 
	```python
	bf = BeautifulSoup(html, 'lxml')
	targets_url = bf.find_all('a', target="_blank")
	```
		
6. Get link contend

	```python
	news_title_link = {}
	for url in targets_url:
	    url_url = BeautifulSoup(str(url), 'lxml')
	    news_title_link[url.getText()] = url_url.a.get('href')
	```

7. Further step into the target link and crawl news contend

	```python
	if 'sinaNews' not in os.listdir():
	    os.makedirs('sinaNews')
	for news_title_key in news_title_link.keys():
	    news_link = news_title_link[news_title_key]
	    contend_req = requests.get(url=news_link, headers=headers)
	    contend_req.encoding = 'utf-8'
	    html = contend_req.text
	    bf = BeautifulSoup(html, 'lxml')
	    contend_targ = bf.find_all('div', id="artibody")
	    if len(contend_targ) != 0:
	        contend = BeautifulSoup(str(contend_targ), 'lxml')
	        conted_list = contend.find_all('p')
	        txt_contend = 'Sina news contend By spider:::::' + news_link
	        for text in conted_list:
	            txt_contend += ('\n' + text.getText())
	        txt_file = open('sinaNews/' + news_title_key + '.txt','w')
	        txt_file.write(txt_contend)
	        txt_file.close()
	```       

## The full version is below:

```python 

# -*- coding:UTF-8 -*-
import os

import requests
from bs4 import BeautifulSoup

url_link = "http://news.sina.com.cn/"
if __name__ == '__main__':
    list_url = []
    headers = {
        "User-Agent": "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36"
    }
    req = requests.get(url=url_link, headers=headers)
    req.encoding = 'utf-8'
    html = req.text
    bf = BeautifulSoup(html, 'lxml')
    targets_url = bf.find_all('a', target="_blank")
    news_title_link = {}
    for url in targets_url:
        url_url = BeautifulSoup(str(url), 'lxml')
        news_title_link[url.getText()] = url_url.a.get('href')

    if 'sinaNews' not in os.listdir():
        os.makedirs('sinaNews')

    for news_title_key in news_title_link.keys():
        news_link = news_title_link[news_title_key]
        contend_req = requests.get(url=news_link, headers=headers)
        contend_req.encoding = 'utf-8'
        html = contend_req.text
        bf = BeautifulSoup(html, 'lxml')
        contend_targ = bf.find_all('div', id="artibody")
        if len(contend_targ) != 0:
            print(contend_targ)
            contend = BeautifulSoup(str(contend_targ), 'lxml')
            conted_list = contend.find_all('p')
            print(conted_list)
            txt_contend = 'Sina news contend By spider:::::' + news_link + '\n'
            for text in conted_list:
                txt_contend += ('\n' + text.getText())
            txt_file = open('sinaNews/' + news_title_key + '.txt', 'w')
            txt_file.write(txt_contend)
            txt_file.close()

```
